export default function ({ store, redirect, route }) {
	if (route.path === '/login') return
	if (!store.state.auth.loggedIn) return redirect('/login')
}
