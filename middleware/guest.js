export default function ({ store, redirect }) {
	if (store.state.auth.loggedIn) {
		// return redirect(`/u/${store.state.auth.user.username}`);
		return redirect('/moshtaries')
	}
}
